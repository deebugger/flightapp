package com.dror.flightapp.web;

import com.dror.flightapp.Main;
import com.dror.flightapp.service.CouponService;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashSet;

/**
 * Created by dbengai on 18/09/2016.
 */

@Controller
@EnableAutoConfiguration
@ComponentScan
public class FlightAppController {

    CouponService couponService;

    @RequestMapping("/")
    @ResponseBody
    String home() {
        return "<html><head><title>Flight App</title></head><body><h1>Flight App</h1>"
                + "<p><a href='/ticket/123'>Check your ticket</a></p>"
                + "<p><a href='/baggage/123/Rome'>Check-in your baggage</a></p>"
                + "<p><a href='/coupon/123/85.6'>Check your coupon (not valid)</a></p>"
                + "<p><a href='/coupon/2/85.6'>Check your coupon (valid)</a></p>"
                + "</body></html>";
    }

    @ResponseBody
    @RequestMapping(value="/ticket/{ticketId}", method= RequestMethod.GET)
    String checkTicket(@PathVariable Long ticketId) {
        String res =  "Ticket [" + ticketId + "] is ";
        res += System.currentTimeMillis() % 2 == 0 ? "available" : "not available";

        return res;
    }

    @ResponseBody
    @RequestMapping(value="/baggage/{baggageId}/{destinationId}", method= RequestMethod.GET)
    String checkinBaggage(@PathVariable Long baggageId, @PathVariable String destinationId) {
        String res = "Check-in for baggage [" + baggageId + "] to " + destinationId + ": ";

        res += System.currentTimeMillis() % 2 == 0 ? "Success!" : "Failure..";

        return res;
    }

    @ResponseBody
    @RequestMapping(value="/coupon/{couponId}/{price:.+}", method= RequestMethod.GET)
    String checkCoupon(@PathVariable Long couponId, @PathVariable Double price) {
        if (couponService.isCouponValid(couponId)) {
            return "Coupon ["
                    + couponId
                    + "] is valid! Your price has been updated to "
                    + couponService.getDiscount(price)
                    + " dollars!";
        } else {
            return "Coupon ["
                    + couponId
                    + "] is not valid - you'll just have to pay those "
                    + Double.toString(price) +
                    " dollars..";
        }
    }

}
