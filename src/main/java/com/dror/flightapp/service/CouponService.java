package com.dror.flightapp.service;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Service;

import java.util.HashSet;

/**
 * Created by dbengai on 18/09/2016.
 */

@Service
@EnableAutoConfiguration
public class CouponService {

    private static HashSet<Long> validCoupons;

    static {
        // initialize valid coupons
        validCoupons = new HashSet<Long>();
        validCoupons.add(1l);
        validCoupons.add(2l);
        validCoupons.add(3l);
    }

    public boolean isCouponValid(long couponId) {
        return validCoupons.contains(couponId);
    }

    public double getDiscount(double price) {
        switch((int) (System.currentTimeMillis() % 3)) {
            case 0:
                return price * 0.1;
            case 1:
                return price * 0.5;
            default:
                return price * 0.6;
        }
    }

}
